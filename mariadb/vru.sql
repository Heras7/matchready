DROP DATABASE IF EXISTS VRU;
CREATE DATABASE VRU CHARACTER SET UTF8 COLLATE UTF8_BIN;
USE VRU;
DROP TABLE IF EXISTS ensayos;
CREATE TABLE ensayos ( apellidos varchar(80), nombre varchar(40), club varchar(80), ensayo int, ensayo_castigo int, transformacion int, puntapie_castigo int, de_drop int, puntuacion_total int, equipo varchar(80) );
DROP TABLE IF EXISTS minutos;
CREATE TABLE minutos ( apellidos varchar(80), nombre varchar(40), club varchar(80), titular int, reserva int, total int, equipo varchar(80) );


LOAD DATA INFILE '/tmp/Ensayos.csv'
INTO TABLE ensayos
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;


LOAD DATA INFILE '/tmp/Minutos.csv'
INTO TABLE minutos
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

